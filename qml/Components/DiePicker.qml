import QtQuick 2.4
import Lomiri.Components 1.3

Item {
    property alias text: label.text
    property alias num: die.num
    property alias values: die.values
    property alias value: die.value

    signal clicked()
    signal pressAndHold()

    onValuesChanged: {
        if (values != null) {
            if (typeof value[0] == 'string' && values[0].indexOf("]") > 0) {
                var valuePos = 0;
                valuePos = values[0].indexOf("]")
                die.labelColor = values[0].substr(1, valuePos - 1)
            }
        }
    }

    Die {
        id: die

        width: units.gu(5)
        height: units.gu(5)
        anchors {
            top: parent.top
            horizontalCenter: parent.horizontalCenter
        }
    }

    Label {
        id: label

        anchors {
            right: parent.right
            bottom: parent.bottom
            left: parent.left
        }

        horizontalAlignment: Label.AlignHCenter
    }

    MouseArea {
        anchors.fill: parent
        onClicked: parent.clicked()
        onPressAndHold: parent.pressAndHold()
    }
}
