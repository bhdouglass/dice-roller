import QtQuick 2.4
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3

Dialog {
    id: simplePopup

    property alias text: label.text

    Label {
        id: label
        horizontalAlignment: Label.AlignHCenter
        wrapMode: Text.WordWrap
    }

    Button {
        text: i18n.tr('Ok')
        color: LomiriColors.orange

        onClicked: PopupUtils.close(simplePopup);
    }
}
